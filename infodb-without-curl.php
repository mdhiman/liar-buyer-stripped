<?php

function get_user_location($ip)
{
//	$ip = "173.2.91.145";
        $output = file_get_contents("http://api.ipinfodb.com/v3/ip-city/?key=f9b4cf1b5591eb8839255c82038cf6fdf1bf3e0b4913f4eb6ea7887f8053d361&ip=" . $ip, "r");
  	$return_values = explode(";", $output);
	$country = $return_values[4];
	$state   = $return_values[5];
	$city    = $return_values[6];
	return array($city, $state, $country);
}

function draw_map($ip)
{
	//$ip = "173.2.91.145";
	$location = get_user_location($ip);
	$city = $location[0];
	$state = $location[1];
	$country = $location[2];

	//print $city . $state . $country;
	$map_url = "http://maps.googleapis.com/maps/api/staticmap?center=" . $city . "," . $state . "&zoom=10&size=300x300&maptype=roadmap&sensor=false";
	array_push($location, $map_url);
	return $location;
	
}

//$o = draw_map("173.2.91.145");
//print_r ($o);       
?>
