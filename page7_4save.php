<?php
        
    include "recordtimings.php";
    global $con;

    if(!isset($_SESSION))   
        session_start();

    $_SESSION['temptdecisionend'] = getTimeStamp();

    $condname = $_SESSION['condname'];
    //echo $condname;
    $_SESSION['conditionstart'] = getTimeStamp();
    if(strcmp('aln', $condname)==0) //all
    {
	   header("location: page8E.php");
    }
    else if(strcmp('al1n', $condname)==0) //all
    {
        //excluded exclamation
        header("location: page8E1.php");
    }
    else if(strcmp('al2n', $condname)==0) //all
    {
        //included exclamation
        header("location: page8E2.php");
    }
    else if(strcmp('ntn', $condname)==0) //nothing
    {
        //just normal presentation -- control group
	    header("location: page8A.php");
    }else if(strcmp('oin', $condname)==0) //only ip
    {
        //just map presentation
	    header("location: page8B.php");
    }else if(strcmp('oln', $condname)==0) //only location
    {
        //only location name
	    header("location: page8C.php");
    }else if(strcmp('omn', $condname)==0) //only map
    {
	    //header("location: page8D.php");
        echo "There is a problem! sorry. We can not proceed with om.";
    }else if(strcmp('ptn', $condname)==0) // pre-test
    {
	    header("location: page8E.php");
    }else
    {	
        $_SESSION['condname'] = "ntm";
        header("location: page8A.php");
    	//echo "There is a problem! sorry. We can not proceed.";
    }
?>
