<?php
    include "settings.php";
    include "recordtimings.php";

    if(!isset($_SESSION))	
    	session_start();

    $_SESSION['temptdecisionstart'] = getTimeStamp();
    
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
   "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<title>
	Go to resolution center!
</title>
<script type="text/javascript" src="general.js"></script>
<script type="text/javascript" src="./js/spin.js"></script>

<script>
    function check_submit()
    {
        var form1 = document.getElementById("form1");
        form1.submit();
    }
</script>
<link media="screen" rel="stylesheet" type="text/css" href="global.css"/>
</head>
<body onload="back_control()">
<table style="width:100%;">
<tr>
<td class="instbox">
	<h1 style="color:black"><center>While waiting for the delivery, you have time to think.</center></h1> 
</td>
</tr>
<tr>
	<td><center>
        Time passes. While you wait for the delivery, you think. <br/>
        Did you do the right thing?<br/>
        Will you get in trouble?<br/>
        What is fair?<br/>
	What is the right choice?<br/>
        <!--Should you just return it, to show that you are a better person than Alex is? But then you lose. What is the right choice?<br/>-->
        
        And then the delivery arrives ... so now you have to decide what to do</center> </br></br>
    </td>
</tr>
<tr>
    <td><center>
        <form action="page7_4save.php" type="post" name="form1" id="form1">
                <input type="button" onclick="check_submit()" class="button primary" value="Continue" style="font-size:16px;"/>
        </form>
	</center>
    </td>
</tr>
</table>
</body>
</html>

