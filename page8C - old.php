<?php
    include "recordtimings.php";
    include "infodb-without-curl.php";
    global $con;

    if(!isset($_SESSION))   
        session_start();

    //$ipnow = $_SERVER['REMOTE_ADDR'];
    //echo $ipnow;
    //if(strcmp("127.0.0.1",$ipnow)==0)
    //    $ipnow = "173.2.91.145";
    $locinfo = draw_map($ipnow);

    $_SESSION['conditionstart'] = getTimeStamp(); 
    
    //read the location of the user from database

    /*$workerid = $_SESSION['workerid'];
    $query = "select * from locationsvalue where workerid='".$workerid."'";

    $result = mysqli_query($con, $query);

    if($row = mysqli_fetch_array($result))
    {
        $loc = $row['finalvalues'];
        $locs = explode(",",$loc);
        $_SESSION['state'] = $locs[0];
        $_SESSION['city'] = $locs[1];
    }else
    {
        die("There is a problem in your HIT. Please contact the requester.");
    }

    $_SESSION['country'] = '-';*/
    //$_SESSION['state'] = $locinfo[1];
    //$_SESSION['city'] = $locinfo[0];


?>


<html>
<head>
<style>
    table#transdetails td{
        color:#6E6E6E;
        background-color:#E0ECF8;
    }
    table#transdetails
    {
        border:1px solid rgb(200,200,200) ;
        background-color:#E0ECF8;
    }
</style>
<title>
	Resolution Center
</title>
<link media="screen" rel="stylesheet" type="text/css" href="global.css">
<script type="text/javascript" src="general.js"></script>
<script type="text/javascript">
function btnconf()
{
    var op1 = document.getElementById("op1").checked;
    var op2 = document.getElementById("op2").checked;
    var op3 = document.getElementById("op3").checked;
    
    if(!op1 && !op2 && !op3)
        alert("You have to choose one option.");
    else
    {
        //var conf = confirm("Are you sure you want to submit?");
        //if(conf)
        //{
            form1 = document.getElementById("form1");
    	    form1.submit();
        //}else
        //{
        //    var val;
        //    if(op1)
        //        val = document.getElementById("op1").value;
        //    else if(op2)
        //        val = document.getElementById("op2").value;
        //    else if(op3)
        //        val = document.getElementById("op3").value;

        //    if(document.getElementById("prevact").value=='')
        //        document.getElementById("prevact").value = val;
        //    else
        //        document.getElementById("prevact").value = document.getElementById("prevact").value+
        //        ","+val;
        //}
    }
}
function cancel_confirm()
{
    //result = confirm("Are you sure you want to cancel the dispute?");
    //if(result)
    //{
        document.getElementById("cdispute").value="yes";
        var form1 = document.getElementById("form1");
        form1.submit();
    //}else
    //{
        //document.getElementById("cdispute").value="no";
    //}
    
}
function inittime()
{
    var localtime = document.getElementById("localtime");
    var ltime = document.getElementById("ltime");
    var timestr = getClientTime();
    ltime.value = timestr;
    localtime.innerText = timestr;
    localtime.textContent = timestr;
    //alert(timestr);
}
</script>
</head>
<body style="background-color:silver"   onload="back_control();inittime()">
<form action="page8save.php" id="form1" name="form1" method="post"/>
<table style="width:100%;">
<tr>
	<td>
<table style="width:100%;border-bottom:thin solid black">
    <tr>
    <td style="width:100%">
        
        <?php
 echo "<img src='avatars-png/" . $_SESSION['avatar'] . ".png' style='float:right'/>";
 ?>
        <h1>Resolution Center</h1>
        <p>Please review the detials of transaction and indicate the reason for dispute.</p>
        <strong>Transaction Information</strong>
        <table id="transdetails">
                <tr>
                    <td style="text-align:right;width:200px">Transaction ID:</td><td style="width:200px"><strong>8b65-0983</strong></td>
                </tr>
                <tr>
                    <td style="text-align:right">Seller Name:</td><td><strong>eBay Inc.</strong></td>
                </tr>
                <tr>
                    <td style="text-align:right">Transaction Amount:</td><td><strong>-$549.99 USD</strong></td>
                </tr>
                <tr>
                    <td style="text-align:right">Item:</td><td><strong>3D WiFi Smart Home Theater System <br/>w Wireless Speakers</strong></td>                </tr>
                <tr>
                    <td style="text-align:right">Transaction Date:</td><td><strong><?php echo $_SESSION['ltime'] ?></strong></td>
                </tr>
		<tr>
                    <td style="text-align:right">Transaction is done from this location:</td><td><strong>
                    <?php
                        echo $_SESSION['state'].", ".$_SESSION['city'];
                    //echo $locinfo[0].", ".$locinfo[1];
                    ?>
                    </strong>
                    </td>
                </tr>
        </table>
        <strong>I am opening this dispute becuase:</strong>
        <table>
        <tr>
        <td>
        <input type="radio" id="op1" name="op" value="nreceive">
                I haven't recieved my item and want a refund.
        </input>
        </td>
        </tr>
        <tr>
        <td>
        <input type="radio" id="op2" name="op" value="damaged">
                I received my item, but it is significantly not as described, and I want to return it.
        </input>
        </td>
        </tr>
        <tr>
        <td>
        <input type="radio" id="op3" name="op" value="norder">
                I did not order this item and want a refund.
        </input>
        </td>
        </tr>
        <tr>
            <td colspan=2>
                <input type="button" value="Confirm Dispute" class="button primary" style="float:right" onclick="btnconf();"/>
		        <input type="button" value="Cancel Dispute" style="float:right" onclick="cancel_confirm()"/>
            </td>
        </tr>
        </table>
        <br/>
        
    </td>
    </tr>

 </table>
	</td>
</tr>
</table>
<input type="hidden" name="prevact" id="prevact"/>
<input type="hidden" name="cdispute" id="cdispute" value="no"/>
</form>
</body>
</html>
