<?php
    include "recordtimings.php";
    include "settings.php";
    global $con;
    if(!isset($_SESSION))
        session_start();
//Design:: We should also think about the significance of these items. For example, why do we need age? why do we need the gender information?
//Is it because of it being a "generic information" not to put them off or do we want to use them as well? What other pieces of information do we need?

   $name = $_POST["name"];
   $age  = $_POST["age"];
   $sex  = $_POST["sex"];
   $ltime = $_POST["ltime"];

   $ip = $_SESSION['ip'];   //= $_SERVER['REMOTE_ADDR'];
   $ua = $_SERVER['HTTP_USER_AGENT'];
   //$_SESSION['ip'] = $ip;

   if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
    $proxyip = $_SERVER['HTTP_X_FORWARDED_FOR']; 
   else
    $proxyip = 'NP';

    $hitid = 'N';
    $workerid = 'N';
    $assignmentid = 'N';
    $expid = -1;
    $condid = -1;
    $condname = 'N';

    if(isset($_SESSION['hitid']))
        $hitid = $_SESSION['hitid'];

    if(isset($_SESSION['workerid']))
        $workerid = $_SESSION['workerid'];
    
    if(isset($_SESSION['assignmentid']))
        $assignmentid = $_SESSION['assignmentid'];

    if(isset($_SESSION['expid']))
        $expid = $_SESSION['expid'];

    if(isset($_SESSION['condid']))
        $condid = $_SESSION['condid'];

    if(isset($_SESSION['condname']))
        $condname = $_SESSION['condname'];
    
//Design:: What extra information should we store in the DB. For example: IP, UA etc.

    if (mysqli_connect_errno($con))
        die ("Failed to connect to MySQL: " . mysqli_connect_error());
     
    
    $insert_query = "INSERT INTO user_record (name, age, sex, ip, ipproxy, ua, hitid, workerid, assignmentid, expid, condid, condname) VALUES ".
    " ('" . mysqli_real_escape_string($con,$name) . "', '" . mysqli_real_escape_string($con,$age) . "', '" . mysqli_real_escape_string($con,$sex) . "'".
        ",'".$ip."','".$proxyip."','".$ua."','".$hitid.
        "','".$workerid."','".$assignmentid."'".
        ", ".$expid.",".$condid.",'".$condname."')";

    //echo $insert_query;

    $result2 = mysqli_query($con, $insert_query);
  
    $subjectunqid = mysqli_insert_id($con);
    $_SESSION['uunqid'] = $subjectunqid;

    $tunqid = initial_timing($subjectunqid);
    $_SESSION['tunqid'] = $tunqid;

    if (!$result2)
        die("Error: XXX" . mysqli_error($con));

    mysqli_close($con);

    $_SESSION['name'] = $name; //Set Session variable
    $_SESSION['ltime'] = $ltime;

    //TODO: fetch the user information about the location from previous experiment
    //check the previous IP with this one to see if user is from the same ip. if that's OK, then we will show the place.
    //or should we use their data from previous experiment, anyway. 

    header('Location: page2.php');
?>
