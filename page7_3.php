<?php
    include "recordtimings.php";
    global $con;

    if(!isset($_SESSION))   
        session_start();

    $_SESSION['fakeorderstart'] = getTimeStamp(); 

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
   "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
    <script type="text/javascript" src="js/jquery.js"></script>

    <link rel="stylesheet" href="./fancybox/source/jquery.fancybox.css" type="text/css" media="screen" />
    <script type="text/javascript" src="./fancybox/source/jquery.fancybox.pack.js"></script>

    <link rel="stylesheet" href="./fancybox/source/helpers/jquery.fancybox-buttons.css" type="text/css" media="screen" />
    <script type="text/javascript" src="./fancybox/source/helpers/jquery.fancybox-buttons.js"></script>
    <script type="text/javascript" src="./fancybox/source/helpers/jquery.fancybox-media.js"></script>

    <link rel="stylesheet" href="./fancybox/source/helpers/jquery.fancybox-thumbs.css" type="text/css" media="screen" />
    <script type="text/javascript" src="./fancybox/source/helpers/jquery.fancybox-thumbs.js"></script>
    
    <script type="text/javascript">
    //    $(document).ready(function() {
    //    $(".fancybox").fancybox({
    //        openEffect  : 'none',
    //        closeEffect : 'none'
    //    });
    //});

        $(document).ready(function() {
            $(".fancybox-thumb").fancybox({
                prevEffect  : 'none',
                nextEffect  : 'none',
                helpers : {
                    title   : {
                        type: 'outside'
                    },
                    thumbs  : {
                        width   : 50,
                        height  : 50
                    }
                }
            });
    });
    </script>

<title>
	Order a new item from eBay!
</title>
<script type="text/javascript" src="general.js"></script>
<script type="text/javascript" src="./js/spin.js"></script>

<script>
    function check_submit()
    {
        //alert("Hello");
        var form1 = document.getElementById("form1");
        form1.submit();
    }
</script>
<link media="screen" rel="stylesheet" type="text/css" href="global.css"/>
</head>
<body onload="back_control()">
<table style="width:100%;">
<tr>
<td class="instbox">
	<h1 style="color:black"><center>Buy a new item from eBay!</center></h1> 
    <center>
        <h2>You buy an item from another seller, thinking that after it arrives you will report that you did not receive it.</h2>
    </center>
</td>
</tr>
<tr>
	<td>
        <br/>
        <br/>
        <center>
            <img src="./images/hometheater2.png" usemap="#productlisting"/>

            <map name="productlisting">
                <area shape="rect" style="cursor: pointer;" coords="430,250,550,300" href="javascript:check_submit()" alt="Buy">               
                <area shape="rect" class="fancybox-thumb" rel="fancybox-thumb" href="avatar-img/hometheaterbig.jpg" style="cursor: pointer;" coords="0,0,310,310" alt="See more photos"/>
            </map>

        </center>
    </td>
</tr>
<tr>
    <td>
        <form action="page7_3save.php" type="post" name="form1" id="form1">
                <!--
                <input type="button" onclick="check_submit()" class="button primary" value="Buy and ship!" style="float:right" />
                -->
        </form>
    </td>
</tr>
</table>
</body>
</html>

