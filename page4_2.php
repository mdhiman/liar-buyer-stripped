<?php
    include "settings.php";
      include "recordtimings.php";

    if(!isset($_SESSION))   
        session_start();
  
    $_SESSION['storypend'] = getTimeStamp();
?>    
<html>
<head>
<style>
#mydiv {
    position:fixed;
    top: 50%;
    left: 50%;
    width:30em;
    height:18em;
    margin-top: -9em; 
    margin-left: -15em; 
    text-align: right;
}

#mydiv span {
   position: absolute;
   bottom: 0;
   right: 50;
}
</style>
<script type="text/javascript" src="./js/spin.js"></script>
<script type="text/javascript">
	function waiting()
	{
		var target = document.getElementById('mydiv');
		var spinner = new Spinner().spin(target);
		
		setInterval(function(){document.form1.submit()},5000);
	}
</script>
</head>


<body onload="waiting()">
<div id="mydiv"> 
<h1><span>Please wait for your shipment</span></h1>
<form action="page4_3.php" method="post" name="form1" id="form1">
</div>
</body>
</html>
