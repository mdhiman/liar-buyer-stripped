<?php
    include "settings.php";
    include "recordtimings.php";
    include "infodb-without-curl.php";
    global $con;
    if(!isset($_SESSION))
    {
        session_set_cookie_params(3600);
        session_start();
    }
    $_SESSION['expstart'] = getTimeStamp();

    $hitid = '';
    $workerid = '';
    $assignmentid = '';

    if(isset($_GET['hitId']))
    {
        $_SESSION['hitid'] = $_GET['hitId'];
    }

    if(isset($_GET['workerId']))
    {
        $_SESSION['workerid'] = $_GET['workerId'];
        $workerid = $_SESSION['workerid'];
    }

    if(isset($_GET['assignmentId']))
    {
        $_SESSION['assignmentid'] = $_GET['assignmentId'];
    } 

    if(isset($_GET['ei']))
        $_SESSION['expid'] = $_GET['ei'];

    if(isset($_GET['ci']))
        $_SESSION['condid'] = $_GET['ci'];

    if(isset($_GET['cn']))
        $_SESSION['condname'] = $_GET['cn']; 

    $query = "select * from locationsvalue where workerid='".$workerid."'";
    $result = mysqli_query($con, $query);

    $ipnow = $_SERVER['REMOTE_ADDR'];
    $_SESSION['ip'] = $ipnow;

    if($row = mysqli_fetch_array($result))
    {
        $loc = $row['finalvalues'];
        $locs = explode(",",$loc);

        $_SESSION['country'] = 'FROMDB';
        $_SESSION['state'] = $locs[0];
        $_SESSION['city'] = $locs[1];
    }else
    {
        if(strcmp("127.0.0.1",$ipnow)==0)
            $ipnow = "173.2.91.145";
        $locinfo = draw_map($ipnow);

        $_SESSION['country'] = $locinfo[2];
        $_SESSION['state'] = $locinfo[1];
        $_SESSION['city'] = $locinfo[0];
        //die("There is a problem in your HIT. Please contact the requester.");
    }


    //echo   "????".$_SESSION['condname']."!!ppppp";

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
   "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<style>

</style>
<script type="text/javascript" src="js/jquery.js"></script> 
<script type="text/javascript" src="general.js"></script>
<script type="text/javascript"> //Time Spent on the Page

  
    var startTime = 0;

    $(function() { //Initial Time
        startTime = Date.now();
    });


    $(window).on('beforeunload', function() { //Final Time
        
        $.ajax({async: false, // Necessary, because the closing code has
                              // to be suspended until the ajax succeeds
                type: "POST",
                url: 'log.php',
                data: {time: Date.now() - startTime},
                success: function(text) {
                    // This is executed when the response has been received
                    // text is response data
                }
               });
    });

</script>

<title>
	Register
</title>

<script>
function check_and_submit()
{

    var nname = document.getElementById("nname").value;
	//var country  = document.getElementById("country").value;		
	//var city  = document.getElementById("city").value;
    	//var state  = document.getElementById("state").value;
	

	if(nname=='')
	{
		alert("Enter your name.");
		document.getElementById("nname").focus();
		return;
	}else if( document.getElementById('age').selectedIndex==-1 )
	{
		alert("Indicate your age.");
		document.getElementById('age').focus();
		return;
	}else if( document.getElementById('sex').selectedIndex==-1 )
	{
		alert("Indicate your gender.");
		document.getElementById('sex').focus();
		return;
	}
	
	/*else if(country=='')
	{
		alert("Please enter your country.");
		document.getElementById("country").focus();
		return;		
	}else if(city=='')
	{
		alert("Please enter your city.");
		document.getElementById("city").focus();
		return;
	}
    	else if(state=='')
    	{
        	alert("Please enter your state.");
        	document.getElementById("state").focus();
        	return;
    	}*/
	//alert(window.form1);
	//alert(window.form1.id);
	document.form1.submit();
	//else if(email=='')
	//{
	//	alert("Enter a nickname.");
	//	document.getElementById("nname").focus();		
	//}
}

function inittime()
{
    var localtime = document.getElementById("localtime");
    var ltime = document.getElementById("ltime");
    var timestr = getClientTime();
    ltime.value = timestr;
    //localtime.innerText = timestr;
    //localtime.textContent = timestr;
    //alert(timestr);
}
function init()
{
    inittime();
	document.getElementById('age').selectedIndex = -1;
	document.getElementById('sex').selectedIndex = -1;
	//TODO:check if the page accept javascript and cookies. if not, show appropriate message and 
	//tell user that she can not take the test unles enable the JavaScript and Cookies
}
</script>
<link media="screen" rel="stylesheet" type="text/css" href="global.css">
</head>
<body onload="init()">
<table style="width:100%;">
<tr>
    <td class="instbox">
    </td>
    <td>
    </td> 
</tr>
<tr>
	<td>
    </br>
	<h1>Tell us about yourself:</h1>
    <form action="page1save.php" method="post" name="form1" id="form1">
    <table>
    <tr>
    <td>Nickname<span style="color:red">*</span>: </td>
    <td><input type="text" name="name" id="nname" style="width:100%"/></td>
    </tr>
    <tr>
    <td>Age<span style="color:red">*</span>: </td>
    <td>
        <select name="age" id='age' style="width:100%">
            <option value="18-25">18-25</option>
            <option value="25-32">25-32</option>
            <option value="32-45">32-45</option>
            <option value="45-60">45-60</option>
            <option value="60-">Above 60</option>
        </select>

    </td>
    </tr>
    <tr>
    <td>Gender<span style="color:red">*</span>:</td><td>
        <select name="sex" id='sex' style="width:100%">
            <option value="Male">Male</option>
            <option value="Female">Female</option>
        </select>
	</td>
    </tr>
    <!-- <tr>
    <td>Email<span style="color:red">*</span>:</td>
    <td><input type="text" name="email" style="width:100%"/></td>
    </tr> -->

    <!-- <tr>
    <td>City<span style="color:red">*</span>:</td><td>
    <input type="text" name="city" id="city" style="width:100%"></td>
    </tr>

    <tr>
    <td>State<span style="color:red">*</span>:</td><td>
    <input type="text" name="state" id="state" style="width:100%"></td>
    </tr>

    <tr>
    <td>Country<span style="color:red">*</span>:</td><td>
    <input type="text" name="country" id="country" style="width:100%"></td>
    </tr> -->

    <tr>
    <td> </td><td>
    <input type="button" class="button primary" name="sbtn" value="Start" onclick="check_and_submit()" style="float:right;" /></p></td>
    </tr>
    <tr>
    <td>
    </td>

    <td>

     <!--<a href="page2.html"><input type="button" value="START" style="float:right; background-color:yellow;font-size:15px"/></a>-->
    </td>
    </tr>

	</table>

	</td>
</tr>
<input type="hidden" name="ltime" id="ltime"/>
</form>

</table>

</body>
</html>
