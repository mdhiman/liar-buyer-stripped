<?php
    include "recordtimings.php";
    
    if(!isset($_SESSION))    
        session_start();

    $_SESSION['gamepstart'] = getTimeStamp();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
   "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<title>
	Simple Game!

</title>
<link media="screen" rel="stylesheet" type="text/css" href="global.css"/>
<script type="text/javascript" src="general.js"></script>

<script type="text/javascript">
    var MAX_SECONDS = 60; //CHANGE HERE LATER
    var corrects = 0;
    var mistakes = 0;
    var enteredcount = 0;
    //var msgbar;
    function startGame()
    {
        var e1 = document.getElementById("row1");
        var e2 = document.getElementById("row2");
        var e3 = document.getElementById("row0");
        var e4 = document.getElementById("instclick");
        e1.style.visibility = "visible";
        e2.style.visibility = "visible";


        e3.style.visibility = "hidden";
        //e4.style.visibility = "hidden";
        e4.innerText = '';//"* Type as fast as you can.";
        e4.textContent = '';//"* Type as fast as you can.";

        startShowing();
    
        document.getElementById("uinput").focus();
        document.getElementById("gstarttime").value = getTimestamp();
        //alert(getTimestamp());
        //msgbar = document.getElementById("msgbar");

    }

    function endGame()
    {
	   //document.getElementById("gstarttime").value = getTimestamp();
       var e1 = document.getElementById("row1");
       var e2 = document.getElementById("row2");
        //var e3 = document.getElementById("row0");
       e1.style.visibility = "hidden";
       e2.style.visibility = "hidden";
       document.getElementById("nbtn").style.visibility = "visible";

//	   results = document.getElementById('resultbar');
//	   results.style.visibility = 'visible';
//	   var msg = "Number of mistakes: " +mistakes + ", Speed: " + (words.length/(elapsedtime*1.0/60)).toFixed(0) + " words per minute ";
//	   results.innerText = msg ;
//	   results.textContent = msg ;

       var e4 = document.getElementById("instclick");
       e4.innerText = "Finished! Click Next to Continue";
       e4.textContent = "Finished! Click Next to Continue";
    }

    var currentshow = 0;
    var elapsedtime = 0;
    var seq = [8, 9, 1,
                   2, 4, 7,
                   3, 5, 6];
    var words = ["flower", "justice", "absurd", "sad", "debt", "unfair",  "BulleT"
	/*,
        "copy", "paste", "clumsy", 
        "wrong", "broken", "angry",
        "profit","long",
        "money","debt"*/
	];
    var lastval = "";
    var timeInterval;
    function startShowing()
    {
        //window.setInterval(function(){showNext()},1000);
        timeInterval = window.setInterval(function(){updateclock();},1000);
        showNext();

    }
    function showNext()
    {
        
        if(currentshow!=0)
        {
            var e1 = document.getElementById("w"+seq[(currentshow-1)%9]);
            e1.innerText = "";
	    e1.textContent = "";

        }
	     
        msgbar = document.getElementById("msgbar");
	msgbar.style.visibility = 'hidden';

        if(currentshow<words.length )
        {
            var e1 = document.getElementById("w"+seq[currentshow%9]);
            //alert("w"+seq[currentshow]);
            e1.innerText = words[currentshow];
	    e1.textContent = words[currentshow];
            currentshow++;
        }else
	    {
	       endGame();
	    }
    }
    function updateclock()
    {
        var e1 = document.getElementById("clock");
	
        //if(elapsedtime)
        //{
            elapsedtime++;
	    e1.innerText = elapsedtime; //MAX_SECONDS - elapsedtime;
	    e1.textContent = elapsedtime;//MAX_SECONDS - elapsedtime;
        //}
        //if(currentshow >= words.length )
        //{
            //window.clearInterval(timeInterval);
            //var e1 = document.getElementById("row2");
            //var e2 = document.getElementById("nbtn");
            //var e3 = document.getElementById("row1");
            
            //e1.style.visibility = "hidden";
            //e3.style.visibility = "hidden";
            //e2.style.visibility = "visible";
            //score = (corrects*100.0/words.length).toFixed(1);
            //alert(score);
            //alert(" first short task. \n" 
            //    + "Your score: " + (corrects*100.0/words.length).toFixed(1) +"/100");
            //alert("Get Ready for the next task.\r\nAre you ready?");
            //alert("Time's up! End of the first short task.\r\n\r\n\r\nReady for the next task?");

            //document.form_score.score.value=score;
            //document.forms["form_score"].submit();
            //window.location="page4.html";
            //alert("h");
        //}
    }
    function getTheWord()
    {
        var enteredText = document.getElementById("uinput").value;
        var testtxt = "";
        if(currentshow!=0)
        {
            var e1 = document.getElementById("w"+seq[(currentshow-1)%9]);
            testtxt = e1.innerText;
	    testtxt = e1.textContent;
        }else
        {
            var e1 = document.getElementById("w0");
            testtxt = e1.innerText;
	    testtxt = e1.textContent;
        }
        
        if(testtxt==enteredText)
        {
            corrects++;
            lastval = "";
            document.getElementById("uinput").value = "";
            showNext();
        }
        else
        {
            //alert("Wrong! Try again!");
	    msgbar = document.getElementById("msgbar");
	    msgbar.style.visibility = 'visible';
	    mistakes++;
        }
    }
    
    function checkChange()
    {
	//msgbar = document.getElementById("msgbar");
        //msgbar.style.visibility = 'hidden';
        //alert("s");
    }
    function checkKey(e)
    {
    	if (window.event && window.event.keyCode == 13)
            {
                getTheWord();
            }else if(e.keyCode== 13) 
            {
        		//alert("Hello");
        		getTheWord();
            }else
        	{
                msgbar = document.getElementById("msgbar");
    	    msgbar.style.visibility = 'hidden';
        	}
    }
    function check_submit()
    {
    	document.getElementById("eltime").value = elapsedtime;
    	document.getElementById("score").value = mistakes;
        document.getElementById("gendtime").value = getTimestamp();
    	form1 = document.getElementById("form1");
    	form1.submit();
    }
    function showncopy()
    {
        alert("Keep typing!");
    }
</script>
</head>
<body onload="back_control()">
<table style="width:100%;">
<tr>
	<td  class="instbox">
	<center>

    <h1 style="color:black">Small Task</h1>
    <span id="instclick">
    You will be given some words to type as fast as you can. <br/>
    Click "Start" when ready! </span>
    </center>

</tr>
<tr>
    <td><center>
            <input type="button" class="button primary" value="Start" onclick="startGame()" id="row0"/></td>  
    </center></td>
</tr>
<tr style="visibility:hidden" id="row1"><td>
    <center>
    </td></tr>
    </center>
<tr style="visibility:hidden" id="row2">
	<td>


<center>
<?php
 echo "<img src='avatars-png/" . $_SESSION['avatar'] . ".png' />";
 ?>
 </center>
 <center>
    <?php echo "</br><b>" . $_SESSION['name'] . "</b>"; ?>
 </center>



 <center>
    <h5>Elapsed Time: <span id="clock">0</span> seconds </h5>

<table style="width:50%;border:2px solid;text-decoration:bold">
    <tr>
    <td id="w1" style="width:30%; height:50px; font-size:20px" onclick="showncopy()"></td>
    <td id="w2" style="width:30%; height:50px; font-size:20px" onclick="showncopy()"></td>
    <td id="w3" style="width:30%; height:50px; font-size:20px" onclick="showncopy()"></td>
    </tr>
    <tr>
    <td id="w4" style="width:30%; height:50px; font-size:20px" onclick="showncopy()"></td>
    <td id="w5" style="width:30%; height:50px; font-size:20px" onclick="showncopy()"></td>
    <td id="w6" style="width:30%; height:50px; font-size:20px" onclick="showncopy()"></td>
    </tr>
    <tr>
       <td id="w7" style="width:30%; height:50px; font-size:20px" onclick="showncopy()"></td>
       <td id="w8" style="width:30%; height:50px; font-size:20px" onclick="showncopy()"></td>
       <td id="w9" style="width:30%; height:50px; font-size:20px" onclick="showncopy()"></td>
    </tr>
    <tr >
     <!--<td style="border-top:2px solid black" colspan="3">-->
        <td colspan="3">
        <br/>
        <center><input type="text" style="width:50%; font-size:18px" id="uinput" onkeyup="checkChange()" onkeypress="checkKey(event)"/> </center>
        <br/>
        <center><input type="button" value="OK" style="font-size:18px" class="button primary" onclick="getTheWord()"></center></td>
    </tr>
    <tr>
	<td  colspan="3"><span id="msgbar" name="msgbar" style="color:red;visibility:hidden">* Wrong! Try again!</span> </td>
    </tr>
</table>
</center>
	</td>
</tr>
    <tr>
	<!--<td  colspan="3" style='text-align:center'>
	<span id="resultbar" name="resultbar" style="color:gray;text-decoration:bold;visibility:hidden">Well done!</span>
	</td>-->
    </tr>
<tr>
    <td>
       <center><input type="button" value="Next" class="button primary" style="font-size:20px; visibility:hidden" id="nbtn" onclick="check_submit()"/></center>
    </td>
</tr>
</table>
<form action="page3save.php" method="post" name="form1" id="form1">
    <input type="hidden" name="score" id="score" value=""/>
    <input type="hidden" name="altime" id="eltime"/>
    <input type="hidden" name="gstarttime" id="gstarttime"/>
    <input type="hidden" name="gendtime" id="gendtime"/>
</form>

</body>
</html>

