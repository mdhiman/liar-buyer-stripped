function back_control() {
    //alert("Hello");
    if (typeof history.pushState === "function") {
        history.pushState("jibberish", null, null);
        window.onpopstate = function () {
            history.pushState('newjibberish', null, null);
            // Handle the back (or forward) buttons here
            // Will NOT handle refresh, use onbeforeunload for this.
        };
    }
    else {
        var ignoreHashChange = true;
        window.onhashchange = function () {
            if (!ignoreHashChange) {
                ignoreHashChange = true;
                window.location.hash = Math.random();
                // Detect and redirect change here
                // Works in older FF and IE9
                // * it does mess with your hash symbol (anchor?) pound sign
                // delimiter on the end of the URL
            }
            else {
                ignoreHashChange = false;   
            }
        };
    }
}

function getTimestamp()
{
    return new Date().getTime()/ 1000;
}
function getClientTime()
{
        var localTime = new Date();
        var year= localTime.getYear();
        var month= localTime.getMonth() +1;
        switch(month)
        {
                case 1:
                month = "Jan";
                break;
                
                case 2:
                month = "Feb";
                break;
                
                case 3:
                month = "March";
                break;
                
                case 4:
                month = "April";
                break;
                
                case 5:
                month = "May";
                break;
                
                case 6:
                month = "June";
                break;
                
                case 7:
                month = "July";
                break;
                
                case 8:
                month = "Aug";
                break;
                
                case 9:
                month = "Sept";
                break;
                
                case 10:
                month = "Oct";
                break;

                case 11:
                month = "Nov";
                break;

                case 12:
                month = "Dec";
                break;
        }
        var date = localTime.getDate();
        var hours = localTime .getHours();
        var minutes = localTime .getMinutes();
        var seconds = localTime .getSeconds();    

        if(hours<10)
        {
            hours = "0" + hours;
        }

        if(minutes<10)
        {
            minutes = "0" + minutes;   
        }

        if(seconds<10)
        {
            seconds = "0" + seconds;
        }

        timestr = hours+":"+minutes+":"+seconds+" "+
                  month+" "+date+", 2013 ";
        return timestr;
} 
