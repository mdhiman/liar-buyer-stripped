<?php
    include "settings.php";
    include "recordtimings.php";
    global $con;

    if(!isset($_SESSION))   
        session_start();

    $_SESSION['readebaymsgstart'] = getTimeStamp();        
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
   "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<title>
	Reply from customer care!
</title>
<script type="text/javascript" src="general.js"></script>
<script>
	function check_submit()
	{
		document.form1.submit();
	}
</script>
<link media="screen" rel="stylesheet" type="text/css" href="global.css"/>
</head>
<body  onload="back_control()">

<table style="width:100%;height:100%">
<tr>
<td class="instbox">
	<h1 style="color:black"><center>You got a reply! </center></h1> 
    <hr/>
</tr>
<tr>
	<td>
<table style="width:100%">
    <tr>
    <td style="width:50%" align="center" >
    <center><?php
 echo "<img src='avatars-png/" . $_SESSION['avatar'] . ".png'/>";
 ?><br/></center>

    <center>
    <?php
        echo "</br><b>" . $_SESSION['name'] . "</b>";
        //var_dump($_SESSION);
    ?>
    </center>
	

    </td>
    <td style="width:50%" align="center">
    <center><img src="avatar-img/bofficer.jpg"></center>
    <center><b>Customer Care Manager, eBay </b></center>
    </td>
    </tr>
    <tr>
    <td style="width:40%;" id="yourmsg">
        <?php
            //session_start();
            //echo '<center><textarea rows="10" name="msgToeBay" columns="200" style="width:90%" disabled>';
	    echo '<b>Your message:</b></br>';
	    echo '<p>';
            echo $_SESSION["msgToeBay"];
	    echo '</p>';
            //echo '</textarea></center>';
        ?>
        <br/>
    </td>

    

        <td style="width:40%;" id="resp">
            <br/><br/> <b>

        Dear Customer,<br/><br/>

Thank you very much for contacting us and sharing your experience with us. <br/>

As you know, it is difficult for us to know who is at fault when two parties have a disagreement. What we do is to look at the complaint histories of the users, and their reputations. We also review written statements from the two parties.
<br/><br/>
We have reviewed your complaint as well as the response sent by Mr. Alex, and we are sorry to inform you that we cannot settle the dispute in your favor. We therefore ask you to please contact Mr. Hunton on your own to try to resolve matters in a way you feel happy with.<br/><br/> 

As always, it is a pleasure helping you, and we appreciate your understanding in this matter.<br/>
        <br/>
        Have a nice day,<br/>
        Clara Sobernita <br/>
        Customer Care Manager<br/>
        eBay<br/>
        Tel: 800-209-3229<br/>
        Fax: 800-209-3220<br/> </b>
        <p/>
        <form action="page6_2save.php" method="post" name="form1" id="form1" style="float:right" >
            <input type="button" value="Continue" name="btnnext" onclick="check_submit()" class="button primary"/>
        </form>
    </td>
    </tr>
 </table>
</td>
</tr>
</table>

</body>
</html>

