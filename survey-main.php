<?php
    include "recordtimings.php";
    include "infodb-without-curl.php";
    global $con;

    if(!isset($_SESSION))   
        session_start();

    $ipnow = $_SESSION['ip'];
    //= $_SERVER['REMOTE_ADDR'];
    //echo $ipnow;
    //if(strcmp("127.0.0.1",$ipnow)==0)
    //    $ipnow = "173.2.91.145";
    //$locinfo = draw_map($ipnow);

    $_SESSION['surveystart'] = getTimeStamp();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
   "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
	<script type="text/javascript" src="js/jquery.js"></script>
	<title>last page!</title>
	<script>
    function check_submit()
    {
	
	if ($('input[name=q1]:checked').length <= 0)
	{
		alert("Please finish Q1");
		return 0;
	}

	if ($("input[name='q2[]']:checked").length <= 0)
        {                              
                alert("Please finish Q2");
                return 0;    
        }    

	if ($('input[name=q3]:checked').length <= 0)
        {                              
                alert("Please finish Q3");
                return 0;    
        }    

        form1.submit();
    }
</script>
<script type="text/javascript" src="general.js"></script>
<link media="screen" rel="stylesheet" type="text/css" href="global.css"/>
</head>
<body onload="back_control()">
<form name="form1" id="form1" action="survey-pre-save.php" method="post">
<table style="width:100%;">
	<tr>
		<td class="instbox">
			<h1 style="color:black"><center>Last page! Please answer the questions below.</center></h1> 
		</td>
	</tr>

	<tr>
		<td>1- Is it fair to trick people to get even?</td>
	</tr>
	<tr>
		<td>
			<input type="radio" name="q1" id="q1" value="Yes, always"/>Yes, always<br/>
			<input type="radio" name="q1" id="q1" value="Yes, if they hurt somebody you love"/>Yes, if they hurt somebody you love<br/>
			<input type="radio" name="q1" id="q1" value="Yes, sometimes"/>Yes, sometimes<br/>
			<input type="radio" name="q1" id="q1" value="No, never"/>No, never<br/>
			</br>
		</td>
	</tr>
	<tr>
		<td>2- Whom would it be fair to trick to get even (Select one or more?)</td>
	</tr>
	<tr>
		<td>
			<input type="checkbox" name="q2[]" id="q2" value="Anybody"/>Anybody<br/>
			<input type="checkbox" name="q2[]" id="q2" value="An individual who was unfair"/>An individual who was unfair<br/>
			<input type="checkbox" name="q2[]" id="q2" value="Any store that was unfair"/>Any store that was unfair<br/>
			<input type="checkbox" name="q2[]" id="q2" value="PayPal/eBay, if they were unfair"/>PayPal/eBay, if they were unfair<br/>
			<input type="checkbox" name="q2[]" id="q2" value="It is never fair to trick somebody to get even"/>It is never fair to trick somebody to get even<br/>
			<br/>
		</td>
	</tr>

	<tr>
	<td>3- Have you had a similar experience as in the role play story?</td>
	</tr>	

	<tr>
		<td>
<input type="radio" name="q3" id="q3" value="Yes, I was tricked"/>Yes, I was tricked<br/>
<input type="radio" name="q3" id="q3" value="Yes, I tricked somebody"/>Yes, I tricked somebody<br/>
<input type="radio" name="q3" id="q3" value="Yes, I was tricked and then got even"/>Yes, I was tricked and then got even<br/>
<input type="radio" name="q3" id="q3" value="No, but I might consider tricking somebody to get even"/>No, but I might consider tricking somebody to get even<br/>
<input type="radio" name="q3" id="q3" value="No, and I would not consider tricking somebody to get even"/>No, and I would not consider tricking somebody to get even<br/>
<input type="radio" name="q3" id="q3" value="No, and I would stop a friend who wanted to trick somebody to get even"/>No, and I would stop a friend who wanted to trick somebody to get even<br/>
			<br/>	
		</td>
	</tr>
	

	<tr><td>4- Is there anything you would like us to know?</td></tr>
	<tr><td><textarea name="q4" cols="50" rows="7"/></textarea><br/></td></tr>
	<tr><td>
		<input type="button" onclick="check_submit()" class="button primary" value="Submit" style="float:right;font-size:20px;"/>
	</td></tr>
</table>

</form>
</body>
</html>
