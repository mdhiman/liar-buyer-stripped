<?php
	//record user choice

  include "recordtimings.php";
  include "settings.php";
  global $con;
  if(!isset($_SESSION))
    session_start();
  
  $tunqid = $_SESSION['tunqid'];
  $_SESSION['conditionend'] = getTimeStamp();


  //recording timings up to this point
  record_timing($tunqid,"gamepstart", $_SESSION['gamepstart']);
  record_timing($tunqid,"gamepend", $_SESSION['gamepend']);

  record_timing($tunqid,"gamestart", $_SESSION['gamestart']);
  record_timing($tunqid,"gameend", $_SESSION['gameend']);

  record_timing($tunqid,"storypstart", $_SESSION['storypstart']);
  record_timing($tunqid,"storypend", $_SESSION['storypend']);

  record_timing($tunqid,"write2sellerpstart", $_SESSION['write2sellerpstart']);
  record_timing($tunqid,"write2sellerpend", $_SESSION['write2sellerpend']);
  
  record_timing($tunqid,"write2sellerstart", $_SESSION['write2sellerstart']);
  record_timing($tunqid,"write2sellerend", $_SESSION['write2sellerend']);
  
  record_timing($tunqid,"readsellermsgstart", $_SESSION['readsellermsgstart']);
  record_timing($tunqid,"readsellermsgend", $_SESSION['readsellermsgend']);

  record_timing($tunqid,"write2ebaypstart", $_SESSION['write2ebaypstart']);
  record_timing($tunqid,"write2ebaypend", $_SESSION['write2ebaypend']);
  
  record_timing($tunqid,"write2ebaystart", $_SESSION['write2ebaystart']);
  record_timing($tunqid,"write2ebayend", $_SESSION['write2ebayend']);
  
  record_timing($tunqid,"readebaymsgstart", $_SESSION['readebaymsgstart']);
  record_timing($tunqid,"readebaymsgend", $_SESSION['readebaymsgend']);
  
  record_timing($tunqid,"temptingstart", $_SESSION['temptingstart']);
  record_timing($tunqid,"temptingend", $_SESSION['temptingend']);

  record_timing($tunqid,"fakeorderstart", $_SESSION['fakeorderstart']);
  record_timing($tunqid,"fakeorderend", $_SESSION['fakeorderend']);

  record_timing($tunqid,"conditionstart", $_SESSION['conditionstart']);
  record_timing($tunqid,"conditionend", $_SESSION['conditionend']);

  record_timing($tunqid,"temptdecisionstart", $_SESSION['temptdecisionstart']);
  record_timing($tunqid,"temptdecisionend", $_SESSION['temptdecisionend']);

  record_timing($tunqid,"expstart", $_SESSION['expstart']);

    
  //save user behavior
//	$prevact = $_POST['prevact'];
	global $con;
	$unqid = $_SESSION['uunqid'];

	if (isset($_POST['op']))
	{
		$choice = $_POST['op'];

    		$query = "update user_record set decision='".$choice."' ". "where unqid=".$unqid;
    		mysqli_query($con, $query);
	}

/*  $cdispute = $_POST['cdispute'];
  $prevact  = $_POST['prevact'];

  $query = "update user_record set prevdecisions='".$prevact."', ".
   	" caceleddispute='".$cdispute."' where unqid=".$unqid;
  mysqli_query($con, $query); */

  //save geo-location
  $country = $_SESSION['country'];
  $state = $_SESSION['state'];
  $city = $_SESSION['city'];
  $ltime = $_SESSION['ltime'];
  
  $avtid = $_SESSION['avatar'];


  $query = "update user_record set ".
      "country='".$country."', ".
      "state='".$state."', ".
      "city='".$city."', ".
      "clienttime='".$ltime."', ".
      "avatar='".$avtid."' ".
      "where unqid=".$unqid;

  mysqli_query($con, $query);

   	//echo $query;

	//echo "Thanks! The experiment finished!";
	header("location: survey-main.php");
?>
