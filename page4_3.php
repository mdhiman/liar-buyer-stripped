<?php
    include "settings.php";
    if(!isset($_SESSION))	
    	session_start();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
   "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
    <script type="text/javascript" src="js/jquery.js"></script>
    <link rel="stylesheet" href="./fancybox/source/jquery.fancybox.css" type="text/css" media="screen" />
    <script type="text/javascript" src="./fancybox/source/jquery.fancybox.pack.js"></script>
    <link rel="stylesheet" href="./fancybox/source/helpers/jquery.fancybox-buttons.css" type="text/css" media="screen" />
    <script type="text/javascript" src="./fancybox/source/helpers/jquery.fancybox-buttons.js"></script>
    <script type="text/javascript" src="./fancybox/source/helpers/jquery.fancybox-media.js"></script>
    <link rel="stylesheet" href="./fancybox/source/helpers/jquery.fancybox-thumbs.css" type="text/css" media="screen" />
    <script type="text/javascript" src="./fancybox/source/helpers/jquery.fancybox-thumbs.js"></script>

<title>
	Received the TV, finally! 
</title>
<script type="text/javascript" src="general.js"></script>
<script type="text/javascript" src="./js/spin.js"></script>

<script type="text/javascript">
    $(document).ready(function() {
        $(".fancybox-thumb").fancybox({
            prevEffect  : 'none',
            nextEffect  : 'none',
	    autoPlay    : 'true',
	    playSpeed   : '1000',
            helpers : {
                title   : {
                    type: 'outside'
                },
                thumbs  : {
                    width   : 50,
                    height  : 50
                }
            }
        });
});
</script>
<script>
        function sendresp()
        {
		document.form1.submit();
	}
</script>

<link media="screen" rel="stylesheet" type="text/css" href="global.css"/>
</head>
<body onload="back_control()">

<table style="width:100%;height:100%">
<tr>
<td class="instbox">
	<h1 style="color:black"><center>Look at exactly what you have received!</center></h1> 
</tr>
<tr>
    <td>
    <br/><center>
    Finally, your TV arrived! <br/>
    But it is really dirty and scratched up. <br/>
    The TV doesn't look at all like it is new.</br>
    You turn it on and find that it is NOT WORKING.</br>
    </td></center>
</tr>
<tr>
	<td>
<table style="width:100%">
    <td style="width:100%" align="center" >
        
    <tr>

       <td style="width:50%" id="yourmsg">
       
       <p>
		<center><b><font color="red"><b>Click the image to view the condition your TV is in.</b></font></center></br>
            <center>
        <a class="fancybox-thumb" rel="fancybox-thumb" href="avatar-img/bad2/4.jpg"><img src="avatar-img/bad2/4.jpg" alt="" style="width: 500px; height:400px;"/></a>

        <a class="fancybox-thumb" hidden="true" rel="fancybox-thumb" href="avatar-img/bad2/2.jpg"><img src="white.gif" alt="" /></a>
 <!--   <a class="fancybox" rel="group" href="avatar-img/TV_Right_Angle1.jpg"><img src="avatar-img/product3.png" src="http://www.htmlgoodies.com/images/white.gif" alt="" /></a>-->
        <a class="fancybox-thumb" hidden="true" rel="fancybox-thumb" href="avatar-img/bad2/3.jpg"><img alt="" src="white.gif"  /></a>
<!--        <a class="fancybox" rel="group" href="avatar-img/TV_Side1.jpg"><img src="avatar-img/product3.png" alt="" src="http://www.htmlgoodies.com/images/white.gif" /></a>-->
        <a class="fancybox-thumb" hidden="true" rel="fancybox-thumb" href="avatar-img/bad2/5.jpg"><img alt="" src="white.gif"/></a>
	    <a class="fancybox-thumb" hidden="true" rel="fancybox-thumb" href="avatar-img/bad2/1.jpg" ><img alt="" src="white.gif"/>
</a>
          
        </center>
	
       
</br>
<!--</br><center><font color="red">Of course, this kind of thing happens, but <b>WHY DOES IT HAVE TO HAPPEN TO YOU???</b></font></center>
-->
</br> 
            <!--<input type="button" value="Send" style="float:right" onclick="sendresp();"/>-->

    </td>
    </tr>
</table>
<tr>
    <td>
            <form action="page5.php" method="post" name="form1" id="form1" >
                        <input type="button" style="float:right;font-size:20px" class="button primary" value="Continue" onclick="sendresp();"/>
            </form>

    </td>
</tr>
</table>
</body>
</html>

