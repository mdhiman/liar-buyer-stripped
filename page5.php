<?php
    include "settings.php";
    include "recordtimings.php";

    if(!isset($_SESSION))	
    	session_start();

    $_SESSION['write2sellerpstart'] = getTimeStamp();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
   "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<title>
	Send a letter to Alex!
</title>
<script type="text/javascript" src="general.js"></script>
<script type="text/javascript" src="./js/spin.js"></script>

<script>
	function sendresp()
	{
		var msg = document.getElementById('msgToAlex').value;
		if(msg.length<50)
		{
			alert("Message to Alex should be at least 50 characters!");
			return;
		}

        document.getElementById("write2sellerend").value = getTimestamp();
		document.form1.submit();
	}
    var started = false;
    function msgchange()
    {
        if(!started)
        {
            started = true;
            document.getElementById("write2sellerstart").value = getTimestamp();
        }

        document.getElementById("txtcount").innerHTML = document.getElementById("msgToAlex").value.length ;
        if(document.getElementById("msgToAlex").value.length>=50)
            document.getElementById("btnsend").disabled = false;
        else
            document.getElementById("btnsend").disabled = true;

    }
</script>
<link media="screen" rel="stylesheet" type="text/css" href="global.css"/>
</head>
<body onload="back_control()">

<table style="width:100%;height:100%">
<tr>
<td class="instbox">
    <div id="pb1" class="progress" role="progressbar" aria-labelledby="pb1_label" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" tabindex="0">
    </div>

	<h1 style="color:black"><center>Contact seller (Alex)</center></h1> 
</td>
</tr>
<tr>
	<td>
<table style="width:100%">
    <tr>
    <td style="width:100%" align="center" >
        
    <center><?php
         echo "<img src='avatars-png/" . $_SESSION['avatar'] . ".png'/>"; ?>
    </center>

    <center>
    <?php
        echo "<b>" . $_SESSION['name'] . "</b>";
    ?>
    </center>
    </td>

    </br> </br>

    <td style="width:50%" align="center"><br/>
    <!--<img src="avatar-img/seller1.jpg">-->
    </td>
    </tr>
    <tr>

    <form action="page5save.php" method="post" name="form1" id="form1" >
        <td style="width:50%" id="yourmsg">
        </br>
            <p>
            <center>
	    Write a letter to Alex (at least 50 characters), and ask him for an explanation and a refund.
	    </center>
            <p>
        
            <center>
                <textarea rows="10" name="msgToAlex" id="msgToAlex" cols="200" style="width:50%" onkeyup="msgchange()"></textarea>
                <br/>
                Written:<span id="txtcount" name="txtcount">0</span>
            </center>
            <br/>
            <center>
            <input type="button" class="button primary" name="btnsend" id="btnsend" value="Send" onclick="sendresp();" disabled/></center>
            <input type="hidden" name="write2sellerstart" id="write2sellerstart"/>
            <input type="hidden" name="write2sellerend" id="write2sellerend"/>
        </form>
            <!--<input type="button" value="Send" style="float:right" onclick="sendresp();"/>-->

    </td>
    </tr>

 </table>
	</td>
</tr>
<tr>
</tr>
</table>
</body>
</html>

