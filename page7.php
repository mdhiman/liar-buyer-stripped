<?php
    include "recordtimings.php";
    global $con;

    if(!isset($_SESSION))   
        session_start();

    $_SESSION['temptingstart'] = getTimeStamp(); 

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
   "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<title>Idea!</title>
<style>

.just-text
{
	text-align:justify;
	text-justify:inter-word;
}

.bubble{
    background-color: #F2F2F2;
    border-radius: 5px;
    box-shadow: 0 0 6px #B2B2B2;
    display: inline-block;
    padding: 10px 18px;
    position: relative;
    vertical-align: top;
}

.bubble::before {
    background-color: #F2F2F2;
    content: "\00a0";
    display: block;
    height: 16px;
    position: absolute;
    top: 11px;
    transform:             rotate( 29deg ) skew( -35deg );
        -moz-transform:    rotate( 29deg ) skew( -35deg );
        -ms-transform:     rotate( 29deg ) skew( -35deg );
        -o-transform:      rotate( 29deg ) skew( -35deg );
        -webkit-transform: rotate( 29deg ) skew( -35deg );
    width:  20px;
}

.me {
    float: left;   
    margin: 0px 0px 0px 0px;         
}

.me::before {
    box-shadow: -2px 2px 2px 0 rgba( 178, 178, 178, .4 );
    left: -9px;           
}

.you {
    float: right;    
    margin: 0px 0px 0px 0px;         
}

.you::before {
    box-shadow: 2px -2px 2px 0 rgba( 178, 178, 178, .4 );
    right: -9px;    
}

</style>
<link media="screen" rel="stylesheet" type="text/css" href="global.css">
<script type="text/javascript" src="general.js"></script>
<script type="text/javascript">
function conf()
{
}
function check_submit()
{
	form1 = document.getElementById("form1");
	form1.submit();
}
</script>
<link media="screen" rel="stylesheet" type="text/css" href="global.css"/>
</head>
<body  onload="back_control()">
<table style="width:100%;">
	<tr>
    <td class="instbox">
    <center><h1 style="color:black">Alex and eBay screwed you!</br>You think of ways to recover your losses.</h1></br>
    <h2>While really angry, you have an idea!</br>
	Read everything the devil and the angel say. Then click on the one that is most fair.</h2> </br></br>
	</center>
    </td>
    <td>
    </td> 
	</tr>
<tr>
<td>
<center>
	<table border="0" style="background-color:#FFFFFF" width="100%" cellpadding="0" cellspacing="0">
	<tr>
		<td rowspan=2> 
		<div onclick="location.href='page7save.php'" class="bubble you just-text" style="width:180px;cursor:pointer">
			<h4>
			1- You could order another product with a similar price from eBay.
			<br/><br/>
			2- You wait to receive it.
			<br/><br/>
			3- Tell eBay that you never received what you ordered. (Maybe somebody "stole" the box after the delivery man put it outside your home?)
			<br/><br/>
			4- And then you ask for a refund.
			<br/><br/>
			5- Then you are even, and nobody would have to know.
			</h4>
		</div>
		</td>
				<td>
				<img src='avatar-img/choice/devil.jpg' width="100" height="100" style="float:right;cursor:pointer" onclick="location.href='page7save.php'">
				</td>
		<td>
		</td>
		<td><img src='avatar-img/choice/angel.jpg' width="100" height="100" style="float:left;cursor:pointer" onclick="location.href='page7_2save.php'"></td>
		<td rowspan=2>
			<div class="bubble me just-text" style="width:150px;float:left;cursor:pointer" onclick="location.href='page7_2save.php'">
				You are a good person. I am sorry this happened to you. You have to forgive.
			</div> </br>
			<?php
			echo "</br>" . "<img style='vertical-align:top;visibility:hidden' src='avatars-png/" . $_SESSION['avatar'] . ".png'/>";
                        echo "</br>" . "<img style='vertical-align:top;visibility:hidden' src='avatars-png/" . $_SESSION['avatar'] . ".png'/>";
			echo "</br>" . "<img style='vertical-align:top;visibility:hidden' src='avatars-png/" . $_SESSION['avatar'] . ".png'/>";
			?>
		</td>
	</tr>
	<tr>
		<td></td>
		<td>
			<center>
			<?php
			echo "<img style='vertical-align:top;' src='avatars-png/" . $_SESSION['avatar'] . ".png'/>";
			echo "</br>" . "<img style='vertical-align:top;visibility:hidden' src='avatars-png/" . $_SESSION['avatar'] . ".png'/>";
			echo "</br>" . "<img style='vertical-align:top;visibility:hidden' src='avatars-png/" . $_SESSION['avatar'] . ".png'/>";
			?>
			</center>
		</td>

		<td></td>
	<tr>
</table>
</center>
</td>
</tr>
<tr>
	<td>
		<!--<form action="page7save.php" type="post" name="form1" id="form1">
			<input type="button" onclick="check_submit()" class="button primary" value="Continue" style="float:right" />
		</form> -->
</td>
</tr>
</table>
</body>
</html>
