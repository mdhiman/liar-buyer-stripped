<?php
	include "settings.php";
    include "recordtimings.php";
    global $con;

    if(!isset($_SESSION))   
        session_start();

    $_SESSION['write2ebaypstart'] = getTimeStamp();
            
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
   "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<title>
	Write to consumer service!
</title>
<script type="text/javascript" src="general.js"></script>
<script>
function check_submit()
{
	var msg = document.getElementById("msgToeBay").value;
	if(msg.length<50)
	{
		alert("Your message should be at least 50 characters.");
		return;
	}
    document.getElementById("write2ebayend").value = getTimestamp();
	document.form1.submit();
}
var started = false;
function msgchange()
{
    if(!started)
    {
        started = true;
        document.getElementById("write2ebaystart").value = getTimestamp();
    }

    document.getElementById("txtcount").innerHTML = document.getElementById("msgToeBay").value.length ;
    if(document.getElementById("msgToeBay").value.length>=50)
        document.getElementById("btnsend").disabled = false;
    else
        document.getElementById("btnsend").disabled = true;
}
</script>
<link media="screen" rel="stylesheet" type="text/css" href="global.css"/>
</head>
<body onload="back_control()">
<table style="width:100%;height:80%">
<tr> <td class="instbox">
	<h1 style="color:black"><center>Write a letter to customer relations </center></h1> 
</td></tr>

<tr>
	<td>
<table style="width:100%">
    <tr>
    <td style="width:50%" align="center" >
        
        
    <center><?php
 echo "<img src='avatars-png/" . $_SESSION['avatar'] . ".png' />";
 ?><br/></center>

 <center>
    <?php
        echo "</br><b>" . $_SESSION['name'] . "</b>";
    ?>
    </center>

    </tr>
    <tr>
    <form action="page6save.php" method="post" style="float:right" name="form1" id="form1">
    <td style="width:50%;" id="yourmsg">
        <p>
            <center>
	If you did not receive a convincing response from Alex; </br>
	you can write a <strong>complaint</strong> and ask <strong>eBay</strong> to help you get your money back. </br>
	(At least 50 characters)</center>
        <p>
    	
        <center><textarea rows="10" name="msgToeBay" id="msgToeBay" cols="200" style="width:50%" onkeyup="msgchange()"></textarea>
            <br/>
            Written:<span id="txtcount" name="txtcount">0</span>
        </center>
        <br/>
        <center>
        <input type="button" name="btnsend" id="btnsend" value="Send" onclick="check_submit()" class="button primary" disabled/></center>
        <input type="hidden" name="write2ebaystart" id="write2ebaystart"/>
        <input type="hidden" name="write2ebayend" id="write2ebayend"/>
    </td>
    </form>
 </table>
	</td>
</tr>
</table>
</body>
</html>
